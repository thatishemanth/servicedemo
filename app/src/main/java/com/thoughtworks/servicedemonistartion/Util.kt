package com.thoughtworks.servicedemonistartion

import android.util.Log

object Util {
    val threadId: Long
        get() {
            val t = Thread.currentThread()
            return t.id
        }
    val threadSignature: String
        get() {
            val t = Thread.currentThread()
            val l = t.id
            val name = t.name
            val p = t.priority.toLong()
            val gname = t.threadGroup.name
            return ( ", Thread Name= "+name + ", "+
                    "Thread ID= " + l + ", "+
                    "Thread Priority= " + p + ", "+
                    "Thread group= " + gname)
        }

//    fun logThreadSignature(tag: String) {
//        Log.d(tag + "THRD-->", threadSignature)
//    }

    fun sleepForInSecs(secs: Int) {
        try {
            Thread.sleep((secs * 1000).toLong())
        } catch (x: InterruptedException) {
            throw RuntimeException("interrupted", x)
        }

    }
}