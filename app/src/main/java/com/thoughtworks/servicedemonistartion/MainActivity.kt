package com.thoughtworks.servicedemonistartion

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import com.thoughtworks.servicedemonistartion.localbinder.MyLocalBinderService
import com.thoughtworks.servicedemonistartion.messengers.MessengerService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.localbinder).setOnClickListener {
            startActivity(
                Intent(
                    this@MainActivity,
                    LocalBinderDemoActivity::class.java
                )
            )
        }
        aidl.setOnClickListener {
            val intent = Intent(this@MainActivity, AIDLDemo::class.java)
            startActivity(intent)
        }

        messages.setOnClickListener{
            Intent(this, MessengerService::class.java).also { intent ->
                startService(intent)
            }

            if (isServiceRunning("com.thoughtworks.servicedemonistartion.messengers.MessengerService")) {
                Log.d("MainActivity", "-->isServiceRunning")
            }

        }


    }

    private fun isServiceRunning(className: String): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (serviceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (className == serviceInfo.service.className) {
                return true
            }
        }
        return false
    }
}
