package com.thoughtworks.servicedemonistartion.localbinder

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.SeekBar
import com.thoughtworks.servicedemonistartion.R
import kotlinx.android.synthetic.main.activity_music_player_client.*

class MusicPlayerClient2 : AppCompatActivity() {

    private lateinit var mService: MyLocalBinderService

    private lateinit var runnable: Runnable
    private var handler: Handler = Handler()

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {

            Log.d("MusicPlayerClient2","--->onServiceConnected"+service.hashCode())
            Log.d("MusicPlayerClient2","--->onServiceConnected")
            val binder = service as MyLocalBinderService.MyLocalBinder
            mService = binder.getService()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d("MusicPlayerClient2","--->onServiceDisconnected")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_player_client)
        Log.d("MusicPlayerClient2","--->onCreate");
        findViewById<Button>(R.id.playBtn).setOnClickListener {
            mService.playAudio()
        }
        findViewById<Button>(R.id.pauseBtn).setOnClickListener {
            mService.pauseAudio()
        }
        findViewById<Button>(R.id.stopBtn).setOnClickListener {
            mService.stopAudio()
        }

        findViewById<SeekBar>(R.id.seek_bar).setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (b) {
                    mService.seekAudio(i)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
    }

    private fun initilizeUI() {
        // Seek bar UI start
        val currentSeconds = mService.currentSeconds
        val diff = mService.seconds - currentSeconds

        seek_bar.max = mService.seconds
        seek_bar.progress = mService.currentSeconds
        tv_pass.text = "$currentSeconds sec"
        tv_due.text = "$diff sec"

        playBtn.isEnabled = mService.playBtnEnabled
        pauseBtn.isEnabled = mService.pauseBtnEnabled
        stopBtn.isEnabled = mService.stopBtnEnabled

    }

    override fun onStart() {
        super.onStart()
        Intent(this, MyLocalBinderService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }

        Log.d("MusicPlayerClient2","--->onStart-->bindService")
        runnable = Runnable {
            initilizeUI()
            handler.postDelayed(runnable, 1000)
        }
        handler.postDelayed(runnable, 1000)
    }

    override fun onStop() {
        super.onStop()
        Log.d("MusicPlayerClient2","--->onStop-->unbindService")
        unbindService(connection)
    }
}
