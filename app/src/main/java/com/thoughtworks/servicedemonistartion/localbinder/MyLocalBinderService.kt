package com.thoughtworks.servicedemonistartion.localbinder

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.thoughtworks.servicedemonistartion.R

class MyLocalBinderService : Service() {

     private val myBinder = MyLocalBinder()

     var pause: Boolean = false
     var playBtnEnabled = true
     var pauseBtnEnabled = false
     var stopBtnEnabled = false

     var timePass = ""
     var timeDue = ""

    var seekBarProgress = 0


    lateinit var mediaPlayer: MediaPlayer


    override fun onCreate() {

        mediaPlayer = MediaPlayer.create(applicationContext, R.raw.sample)
        Log.d("MyLocalBinderService","----->onCreate")
        super.onCreate()
    }

    override fun onBind(intent: Intent): IBinder {
        startService(intent)
        Log.d("MyLocalBinderService","----->onBind-->startService")
        return myBinder
    }

    override fun onDestroy() {
        mediaPlayer.stop()
        Log.d("MyLocalBinderService","----->onDestroy")
        super.onDestroy()

        //You can do restart service if needed by configuring a broadcast receiver and calling it with intent
    }

    override fun onUnbind(intent: Intent?): Boolean {
        //stopSelf()
        //stopService(intent)
        //=======
        //Log.d("MyLocalBinderService","--->onUnbind-->return true")
        //return true // Return true, if want to execute onRebind for next bindings
        //====

        //=====
        Log.d("MyLocalBinderService","--->onUnbind")
        return super.onUnbind(intent)
        //=====
    }

    override fun onRebind(intent: Intent?) {
        Log.d("MyLocalBinderService","--->onRebind")
        super.onRebind(intent)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("MyLocalBinderService","----->onStartCommand")
        return super.onStartCommand(intent, flags, startId)
    }

    inner class MyLocalBinder : Binder() {
        fun getService(): MyLocalBinderService = this@MyLocalBinderService
    }


    fun playAudio() {

        if (pause) {
            mediaPlayer.seekTo(mediaPlayer.currentPosition)
            mediaPlayer.start()
            pause = false

        } else {
            mediaPlayer.start()
        }

        playBtnEnabled = false
        pauseBtnEnabled = true
        stopBtnEnabled = true
    }

    fun pauseAudio() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
            pause = true
            playBtnEnabled = true
            pauseBtnEnabled = false
            stopBtnEnabled = true
        }
    }

    fun stopAudio() {
        if (mediaPlayer.isPlaying || pause.equals(true)) {
            pause = false
            seekBarProgress = 0
            mediaPlayer.stop()
            mediaPlayer.reset()
            mediaPlayer.release()

            playBtnEnabled = true
            pauseBtnEnabled = false
            stopBtnEnabled = false
            timePass = ""
            timeDue = ""
        }
    }

    fun seekAudio(i:Int){
        mediaPlayer.seekTo(i * 1000)

    }


    // Creating an extension property to get the media player time duration in seconds
    val seconds: Int
        get() {
            return mediaPlayer.duration / 1000
        }
    // Creating an extension property to get media player current position in seconds
    val currentSeconds: Int
        get() {
            return mediaPlayer.currentPosition / 1000

        }
}
