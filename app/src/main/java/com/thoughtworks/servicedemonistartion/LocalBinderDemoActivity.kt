package com.thoughtworks.servicedemonistartion

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.thoughtworks.servicedemonistartion.localbinder.MusicPlayerClient1
import com.thoughtworks.servicedemonistartion.localbinder.MusicPlayerClient2

class LocalBinderDemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local_binder_demo)

        findViewById<Button>(R.id.client_one).setOnClickListener {
            startActivity(
                Intent(this@LocalBinderDemoActivity,
                    MusicPlayerClient1::class.java)
            )
        }
        findViewById<Button>(R.id.client_two).setOnClickListener {
            startActivity(
                Intent(this@LocalBinderDemoActivity,
                    MusicPlayerClient2::class.java)
            )
        }
    }
}
