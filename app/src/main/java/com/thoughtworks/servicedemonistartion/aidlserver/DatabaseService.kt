package com.thoughtworks.servicedemonistartion.aidlserver

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.thoughtworks.servicedemonistartion.IMyAidlInterface
import com.thoughtworks.servicedemonistartion.Util

class DatabaseService : Service() {

    private val iMyAidlInterface = Calc()
    override fun onBind(intent: Intent): IBinder {
        return iMyAidlInterface
    }

    class Calc : IMyAidlInterface.Stub() {

        override fun returnMumber(num1: Int, isSleep: Boolean): Int {
            Log.d("DatabaseService", "returnMumber" + Util.threadSignature)
            if (isSleep)
                Util.sleepForInSecs(1)
            return num1
        }

        override fun getPid(): Int {
            return 1
        }
    }


}
