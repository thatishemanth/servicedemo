package com.thoughtworks.servicedemonistartion.messengers


import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import com.thoughtworks.servicedemonistartion.MainActivity
import com.thoughtworks.servicedemonistartion.Util

class MessengerService : Service() {

    private inner class IncomingHandler : Handler() {
        override fun handleMessage(msgFromActivity: Message) {
            reply(msgFromActivity)
        }

        private fun reply(msgFromActivity: Message) {
            val bundle = msgFromActivity.data
            val numOne = bundle.getInt("numOne", 0)

            // Send data back to the Activity
            val incomingMessenger = msgFromActivity.replyTo
            val msgToActivity = Message.obtain(null, 87)

            val bundleToActivity = Bundle()
            bundleToActivity.putInt("result", numOne)

            msgToActivity.data = bundleToActivity
            try {
                if(bundle.getBoolean("isSleep", false))
                      Util.sleepForInSecs(1)
                incomingMessenger.send(msgToActivity)
                Log.d("MessengerService", "Sending message to the activity" + Util.threadSignature)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }
    }

    internal var mMessenger = Messenger(IncomingHandler())

    override fun onBind(intent: Intent): IBinder? {
        Log.d("MessengerService", "-->onBind" + Util.threadSignature)
        return mMessenger.binder
    }

    override fun onStart(intent: Intent?, startId: Int) {
        Log.d("MessengerService", "-->onStart" + Util.threadSignature)
        super.onStart(intent, startId)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("MessengerService", "-->onStartCommand" + Util.threadSignature)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d("MessengerService", "-->unbinding" + Util.threadSignature)
        return super.onUnbind(intent)
    }

    override fun onRebind(intent: Intent?) {
        Log.d("MessengerService", "-->rebinding" + Util.threadSignature)
        super.onRebind(intent)
    }
}
