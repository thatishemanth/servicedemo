package com.thoughtworks.servicedemonistartion

import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.thoughtworks.aidlclient.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async


class MainActivity : AppCompatActivity() {


    private var isServiceBound = false;

    private var addService: IMyAidlInterface? = null


    private val serviceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Log.d("MainActivity--Client", "-->onServiceConnected" + Util.threadSignature)

            addService = IMyAidlInterface.Stub.asInterface(service)
            isServiceBound = true

        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d("MainActivity--Client", "-->onServiceDisconnected" + Util.threadSignature)
            isServiceBound = false
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        fab.setOnClickListener {
            makeAPICall(1, true)
            makeAPICall(2, false)
            makeAPICall(3, true)
            makeAPICall(4, false)
            makeAPICall(5, true)
            makeAPICall(6, true)

//            GlobalScope.async {
//                makeAPICall(1, true)
//            }
//
//            GlobalScope.async {
//                makeAPICall(2, false)
//            }
//
//            GlobalScope.async {
//                makeAPICall(3, true)
//            }
//
//            GlobalScope.async {
//                makeAPICall(4, false)
//            }
//
//            GlobalScope.async {
//                makeAPICall(5, true)
//            }
//
//            GlobalScope.async {
//                makeAPICall(6, true)
//            }
//
//            GlobalScope.async {
//                makeAPICall(7, false)
//            }
//
//            GlobalScope.async {
//                makeAPICall(8, true)
//            }

        }
    }

    private fun makeAPICall(number: Int, isSleep: Boolean) {
        Log.d("MainActivity", "makeAPICall" + Util.threadSignature)

        try {
            if (isServiceBound) {
                result.text = result.text.toString() + addService?.returnMumber(number, isSleep).toString()
            }
        } catch (remoteException: RemoteException) {
            remoteException.printStackTrace()
        }
    }

    override fun onStart() {
        initConnection()
        super.onStart()
    }

    private fun initConnection() {
        if (!isServiceBound) {
            val intent = Intent(IMyAidlInterface.Stub::class.java.name)
            intent.setAction("service.calc")
            intent.setPackage("com.thoughtworks.servicedemonistartion")
            Log.d("", "Binding to the service" + Util.threadSignature)
            bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("", "Unbinding from the service" + Util.threadSignature)
        unbindService(serviceConnection)
    }

    override fun onResume() {
        super.onResume()
        if (!isServiceBound) {
            initConnection()
        }
    }

}
