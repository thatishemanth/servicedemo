package com.thoughtworks.servicedemonistartion

import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import com.thoughtworks.aidlclient.R
import kotlinx.android.synthetic.main.activity_main.*


class MessengerActivity : AppCompatActivity() {
    internal var mIsBound = false
    lateinit var txvResult: TextView
    private lateinit var mService: Messenger


    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.d("MessengerActivity", "-->Service started :$service" + Util.threadSignature)
            mService = Messenger(service)
            mIsBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.d("MessengerActivity", "-->Service destroyed on :$name, because of some reason" + Util.threadSignature)
            mIsBound = false
        }
    }

    inner class IncomingResponseHandler : Handler() {
        override fun handleMessage(msg: Message) {
            Log.d("MessengerActivity", "-->message received $msg" + Util.threadSignature)
            val bundle = msg.data
            val result = bundle.getInt("result", 0)
            txvResult.text = txvResult.text.toString() + "$result \n"

        }
    }

    fun sendMessage(what: Int, message: Int, isSleep: Boolean) {

        val messageToService = Message.obtain(null, what)
        val bundle = Bundle()
        bundle.putInt("numOne", message)
        bundle.putBoolean("isSleep", isSleep)
        messageToService.data = bundle
        messageToService.replyTo = Messenger(IncomingResponseHandler())
        try {
            Log.d("MessengerActivity", "-->sending message")
            mService.send(messageToService)
            Log.d("MessengerActivity", "-->message sent $messageToService" + Util.threadSignature)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        txvResult = findViewById(R.id.txvResult)

        bindMyMessengerService()
        /* if (isServiceRunning("com.thoughtworks.servicedemonistartion.messengers.MessengerService")) {
             Log.d("MainActivity", "-->isServiceRunning")
             bindMyMessengerService()
         }else{
             Log.d("MessengerActivity", "-->ServiceRunning--NOT")

              fab.setOnClickListener() {
            sendMessage(23, 23)
        }

        incrementbutton.setOnClickListener() {
            sendMessage(24, 24)
        }
         }*/

        fab.setOnClickListener() {

            sendMessage(24, 24, false)

            sendMessage(25, 25, true)

            sendMessage(26, 26, false)

            sendMessage(27, 27, true)

            sendMessage(28, 28, false)

            sendMessage(29, 29, true)

        }


    }

    fun bindMyMessengerService() {
        try {
            val intent = Intent("a.b.c.MY_INTENT")
            intent.setClassName(
                "com.thoughtworks.servicedemonistartion",
                "com.thoughtworks.servicedemonistartion.messengers.MessengerService"
            );
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun isServiceRunning(className: String): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (serviceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (className == serviceInfo.service.className) {
                return true
            }
        }
        return false
    }

}
